import { Component,OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-book',
	templateUrl: './book.component.html'
})

export class BookComponent implements OnInit {



	formats:any;
	countries:any;
	cities:any;
	companies:any;

	countrySelected:boolean = false;
	citySelected:boolean = false;
	companySelected:boolean = false;
	formatSelected:boolean = false;

	selectedCountry: any;
	selectedCity:any;
	selectedCompany:any;
	selectedFormat:any;
	onContrySelect:any;
	onCitySelect:any;

	bookForm: FormGroup;
	author:string;
	title:string;
	isbn:number ;
	pages:number;
	description:string;
	price:number;
	formatId:string;
	info:string ;
	city:string;
	country:string;
	companyId:string;

	errorMessage:string = 'This filed is required.'

	constructor(private fb: FormBuilder, private http: HttpClient) {
		this.bookForm = fb.group({
			'author':[null, [Validators.required, Validators.pattern('[A-Za-z\ ]{2,}')]],
			'title':[null, [Validators.required, Validators.pattern('[A-Za-z\ ]{2,}')]],
			'isbn':[null, [Validators.required, Validators.pattern('[0-9]{10}')]],
			'pages':[null, [Validators.required, Validators.min(1),  Validators.pattern('[0-9]{1,}')]],
			'description':[null, [Validators.required, Validators.minLength(30)]],
			'price':[null, [Validators.required, Validators.pattern('[0-9]{1,}\.?[0-9]{0,2}')]],
		})
	}

	ngOnInit() {

		var headers = new HttpHeaders()
			.set('x-auth-token', 'bad18eba1ff45jk7858b8ae88a77fa30');

		this.http.get(' http://localhost:3004/formats', {headers}).subscribe(data => {
			this.formats = data;
		});

		this.http.get(' http://localhost:3004/countries', {headers}).subscribe(data => {
			this.countries = data;
		});

		this.onContrySelect = function onCountrySeect() {
			this.countrySelected = true;
			var id = this.selectedCountry.id;
			this.http.get(' http://localhost:3004/cities', {headers}).subscribe(data => {
				var choosenCities =[];
				for (var key in data) {
					if (data[key].countryId == id) {
						choosenCities.push(data[key])
					}
				}
				this.cities = choosenCities;
			});
		};

		this.onCitySelect = function onCitySelect() {
			this.citySelected = true;
			var id = this.selectedCity.id;
			this.http.get(' http://localhost:3004/publishers', {headers}).subscribe(data => {
				var choosenCompanies =[];
				for (var key in data) {
					if (data[key].cityId == id) {
						choosenCompanies.push(data[key])
					}
				}
				this.companies = choosenCompanies;
			});
		};

	}

onCompanySelect(){
	this.companySelected = true;
};
onFormatSelect(){
	this.formatSelected = true;
}


	addBook(book) {
		this.author = book.author;
		this.title = book.title;
		this.isbn = book.isbn;
		this.pages = book.pages;
		this.description = book.description;
		this.formatId = this.selectedFormat;
		this.price = book.price;
		this.info = book.info;
		console.log('add')
		this.country = this.selectedCountry.name;
		this.city = this.selectedCity.name;
		this.companyId = this.selectedCompany;

		var headers = new HttpHeaders()
			.set('x-auth-token', 'bad18eba1ff45jk7858b8ae88a77fa30');
		this.http.post(' http://localhost:3004/books',{
			id: new Date(),
			author: this.author,
			title: this.title,
			isbn: this.isbn,
			pages: this.pages,
			publisherId: this.companyId,
			formatId: this.formatId,
			description: this.description,
			price: this.price
		}, {headers}
		).subscribe(res => {
			console.log(res)
		});

	}

}
