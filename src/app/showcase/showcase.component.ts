import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http, Headers, RequestOptions} from '@angular/http';

@Component({
	selector: 'app-showcase',
	templateUrl: './showcase.component.html'
})
export class ShowcaseComponent implements OnInit {

	books: any
	formats:string[] = []

	constructor(private http: HttpClient) {}

	ngOnInit() {
		var formats = [];
		var headers = new HttpHeaders()
			.set('x-auth-token', 'bad18eba1ff45jk7858b8ae88a77fa30');
		this.http.get(' http://localhost:3004/books', {headers}).subscribe(data => {
			this.books = data;
		});
		this.http.get(' http://localhost:3004/formats', {headers}).subscribe(data => {
			for (var key in data) {
				this.formats.push(data[key].name) 
			}
		});
	}

}
